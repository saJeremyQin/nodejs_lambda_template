# NodeJS Lambda processor

This is a template file for creating a third party processor for ApiOpenStudio in NodeJS to run as a docker on a Lambda function.

You can run this as a docker on the same (or separate) server, but it is primarily designed to be run as a Lambda function.

**NOTE**: The correct way to get `Traefic` to consume from the hosts `localhost` is very complex.

So until a better way is found, this means that your local development Lambda instance is unreachable from ApiOpenStudio's local development Traefic network.

This means that you will not be able to test your local lambda dorectly with your development ApiOpenStudio instance. Instead, you will need to develop it with the expected stub data, and once you are happy, deploy it to AWS (this is cheap) and the call the remote lambda from a test resource in your development environment.

## Testing

Automated testing are actively encouraged. This will enable stability and confidence in your code.

Here are some general guidelines:

* Use the testing framework of your choice.
* Create a [test strategy and test plan][test-plan] to organise the different test scenarios before coding your tests. 
* Use realistic stub data to test different scenarios.
* Use [Gitlab pipelines][gitlab-pipelines] or [GitHub Actions][github-actions] to automatically run tests on:
   * Commit.
   * Merge request.
   * Tagging.
* Prevent merging if any tests fail.
* Create a test report in the pipelines.

## Using `AWS SAM`

If you prefer to use `AWS SAM` (this will make it easier to use any other languages), you do not need to clone this repository and can create your own.

* Ensure you have the [pre-requisites installed][sam-prerequisites] for `AWS SAM CLI`.
* [Install `AWS SAM CLI`][sam-cli-install].
* Set up a `Hello World` lambda function, see the [tutorial here][sam-hello-world].

## Using this repository

Fork this repository into your own repository.

Edit `index.js` to process the input data and return the result data.

Although `index.js` is the main entry-point, you can separate functionality into other files/directories and functions/classes to ensure readability and maintenance.

### Input data

The input data is the contents of the `event` object.

ApiOpenStudio will send a JSON string of all input parameters in an object, referenced by a key. Form instance:

```json
{
  "my_param1": "foobar",
  "my next parameter": {
    "id": 42,
    "name": "the meaning of life"
  }
}
```

### Test locally

```bash
docker build --platform linux/amd64 -t my-processor:test .
docker run --platform linux/amd64 -d -p 9000:8080 my-processor:test
curl "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{}'
```

Update the `-d` value to contain the data that you want to send to the processor (JSON).

## Test with ApiOpenStudio Core

Once you are happy with your `Lambda`, you can deploy it to AWS, and create a test resource to utilise it.

To consume the Lambda, use the `lambda_function` processor. for example"

```yaml
name: Test lambda function

description: test lambda function.

uri: test/lambda

method: post

appid: 1

ttl: 0

process:
    processor: lambda_function
    id: test lambda function
    function_name:
        processor: var_post
        id: function name
        key: function_name
    iam_key:
        processor: var_post
        id: key get
        key: key
    iam_secret:
        processor: var_post
        id: secret get
        key: secret
    aws_region:
        processor: var_post
        id: region get
        key: region
    data:
        processor: var_post
        id: data get
        key: data
```

### Add to apiopenstudio_docker_dev _**(not working)**_

Add a new service to `docker-cmopose/yml`:

```yaml
my_processor:
  container_name: "${APP_NAME}-my-processor"
  build:
    context: "/absolute/path/to/my-processor"
  ports:
    - "9003:8080"
  labels:
    - traefik.enable=true
    - traefik.docker.network=apiopenstudio_docker_dev_default
    - traefik.http.routers.myprocessor.rule=Host(`my-processor.${DOMAIN}`)
    - traefik.http.routers.myprocessor.tls=true
  networks:
    - default
```

Add the following to your hosts file:

```text
127.0.0.1       my-processor.apiopenstudio.local
```
Once the docker images are running, this can now be referenced at the url:

```bash
curl "http://localhost:9003/2015-03-31/functions/function/invocations" -d '{}'
```

## Deploying the Lambda

### Using `AWS SAM CLI`

See [the documentation][sam-deploy] for deploying your docker using `AWS SAM CLI`.

### Deply manually

To upload the image to Amazon ECR and create the Lambda function, see
[Working with Lambda container images][aws-images-create].

1. Run the [get-login-password][get-login-password] command to authenticate the Docker CLI to your Amazon ECR registry.
    * Set the --region value to the AWS Region where you want to create the Amazon ECR repository. 
    * Replace 111122223333 with your AWS account ID.
    ```bash
    aws ecr get-login-password --region <region> | docker login --username AWS --password-stdin <registryId>.dkr.ecr.<region>.amazonaws.com
    ```
    or (with AWS profile)
    ```bash
    aws ecr get-login-password --profile my_profile | docker login --username AWS --password-stdin 111122223333.dkr.ecr.us-east-1.amazonaws.com
    ```

2. Create a repository in Amazon ECR using the [create-repository][create-repository] command.
    ```bash
    aws ecr create-repository --repository-name <repository-name> --region <region> --image-scanning-configuration scanOnPush=true --image-tag-mutability MUTABLE
    ```
    or (with AWS profile)
    ```bash
    aws ecr create-repository \
    --profile my_profile \
    --repository-name hello-world \
    --region us-east-1 \
    --image-scanning-configuration \
    scanOnPush=true \
    --image-tag-mutability MUTABLE
    ```
   
    **Note:** The Amazon ECR repository must be in the same AWS Region as the Lambda function.
    
    If successful, you see a response like this:
    ```json
    {
      "repository": {
        "repositoryArn": "arn:aws:ecr:us-east-1:111122223333:repository/hello-world", 
        "registryId": "111122223333", 
        "repositoryName": "hello-world", 
        "repositoryUri": "111122223333.dkr.ecr.us-east-1.amazonaws.com/hello-world", 
        "createdAt": "2023-03-09T10:39:01+00:00", 
        "imageTagMutability": "MUTABLE", 
        "imageScanningConfiguration": {
          "scanOnPush": true
        }, 
        "encryptionConfiguration": {
          "encryptionType": "AES256"
        }
      }
    }
    ```

3. Copy the `repositoryUri` from the output in the previous step.

4. [Build][docker-build] and [tag][docker-tag] to tag your local image into your Amazon ECR repository as the latest version. In this command:
   * Replace `docker-image:test` with the name and [tag][tag] of your Docker image. 
   * Replace `<ECRrepositoryUri>` with the `repositoryUri` that you copied. Make sure to include `:latest` at the end of the URI.
    ```bash
    docker build -t docker-image:test -t <ECRrepositoryUri>:latest .
    ```
    Example:
    ```bash
    docker build \
    --platform=linux/amd64 \
    -t hello-world:test \
    -t 111122223333.dkr.ecr.us-east-1.amazonaws.com/hello-world:latest .
    ```

5. Run the [docker push][docker-push] command to deploy your local image to the Amazon ECR repository. Make sure to include :latest at the end of the repository URI.
    ```bash
    docker push <ECRrepositoryUri>:latest
    ```
    Example
    ```bash
    docker push 111122223333.dkr.ecr.us-east-1.amazonaws.com/hello-world:latest
    ```

6. [Create an execution role][create-execution-role] for the function, if you don't already have one. You need the Amazon Resource Name (ARN) of the role in the next step.

7. Create the Lambda function. For ImageUri, specify the repository URI from earlier. Make sure to include :latest at the end of the URI.
    ```bash
    aws lambda create-function \
      --function-name <image_name> \
      --package-type Image \
      --code ImageUri=<ECRrepositoryUri>:<tag> \
      --role arn:aws:iam::<registryId>:role/lambda-ex
    ```
   Example (with profile)
    ```bash
    aws lambda create-function \
      --function-name hello-world \
      --package-type Image \
      --code ImageUri=111122223333.dkr.ecr.us-east-1.amazonaws.com/hello-world:latest \
      --role arn:aws:iam::111122223333:role/lambda-ex \
      --region us-east-1 \
      --profile my_profile
    ```
    **Note:** You can create a function using an image in a different AWS account, as long as the image is in the same Region as the Lambda function. For more information, see Amazon ECR cross-account permissions.

8. Invoke the function.
    ```bash
    aws lambda invoke --function-name hello-world response.json
    ```
    ```bash
    aws lambda invoke --function-name hello-world --profile my_profile response.json
    ```
    You should see a response like this:
    ```json
    {
      "StatusCode": 200,
      "ExecutedVersion": "$LATEST"
    }
    ```

9. To see the output of the function, check the `response.json` file.

To update the function code, you must build the image again, upload the new image to the Amazon ECR repository, and then use the [update-function-code][update-function-code] Add your files command to deploy the image to the Lambda function.

# Links

* https://docs.aws.amazon.com/lambda/latest/dg/nodejs-image.html

[get-login-password]: https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ecr/get-login-password.html
[create-repository]: https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ecr/create-repository.html
[docker-tag]: https://docs.docker.com/engine/reference/commandline/tag/
[docker-build]: https://docs.docker.com/engine/reference/commandline/build/
[tag]: https://docs.docker.com/engine/reference/commandline/build/#tag
[docker-push]: https://docs.docker.com/engine/reference/commandline/push/
[create-execution-role]: https://docs.aws.amazon.com/lambda/latest/dg/gettingstarted-awscli.html#with-userapp-walkthrough-custom-events-create-iam-role
[update-function-code]: https://awscli.amazonaws.com/v2/documentation/api/latest/reference/lambda/update-function-code.html
[aws-images-create]: https://docs.aws.amazon.com/lambda/latest/dg/images-create.html#images-ric
[sam-prerequisites]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/prerequisites.html
[sam-cli-install]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/install-sam-cli.html
[sam-hello-world]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-getting-started-hello-world.html
[sam-deploy]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-deploying.html
[gitlab-pipelines]: https://docs.gitlab.com/ee/ci/pipelines/
[github-actions]: https://docs.github.com/en/actions
[test-plan]: https://www.practitest.com/resource-center/article/write-a-test-plan/
